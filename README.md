# Docker opgaven

- clone deze repo naar je laptop
- start kitematic op
- start een command shell op

We moeten eerst je laptop configureren zodat deze standaard naar de juiste instellingen
van docker kijkt.

- Run `docker-machine env default` om je eigenschappen van docker te bekijken.
- Run `eval "$(docker-machine env default)`

- Het commando `docker` op windows kan nu praten met de `docker daemon` op de virtuele
machine.

## Starten en stoppen

- Ga naar kitematic
- Dubbelklik op hadleyverse: de container zal starten.
- Kijk met de command-line en een `docker`commando of de container actief is
- Stop de container met `docker` commando
- En start de container
- Creeer een nieuwe container gebaseerd op "rocker/hadleyverse" en noem deze "R"
- Druk in `kitematic` op `exec` voor deze container en start R op.
- `print("hello R!")`
- Je hebt een werkende versie van R. Met `Rscript`.

## Docker file

- Bouw de `Dockerfile` met `docker build` en geef het de naam `scraper_lego_i`
- Creeer en start een container `scraper_lego` gebaseerd op dit image.
- Bekijk de Dockerfile

- Pas de `Dockerfile`  aan zodat je brandstofprijzen gescrapet worden
- Bouw een nieuw image `scraper_brandstof_i`
- Start de container.

De resultaten worden op het scherm weggeschreven:
- Pas de `Dockerfile`  aan zodat een scraping rapport gebouwd wordt:
- Voeg hiervoor het sjabloon (<naam>.Rmd) file toe
- Maak een script "build.R" met daarin:

```
library(knitr)
knit2pdf("<naam.Rmd>", "/var/data/rapport.pdf")
```

- Pas het Dockerfile aan:
- Het bouwt nu automatisch een rapport in de "/var/data" directory. 
- Beeld met `kitematic` het `VOLUME` "/var/data" af op een directory van je OS.
- Het rapport wordt nu gegenereerd in deze directory!
