FROM rocker/hadleyverse
MAINTAINER Edwin de Jonge

VOLUME /var/data
WORKDIR /tmp
ADD lego.R .
CMD ["Rscript", "lego.R"]
